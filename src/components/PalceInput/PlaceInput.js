import React, { Component } from 'react';
import {AppRegistry, TextInput, View, Text, Button, StyleSheet, TouchableOpacity} from 'react-native';

class PlaceInput extends Component {
    render() {
      return (
        <View style={styles.inputBox}>
          <TextInput 
            style={styles.placeInput}
            value={this.props.placeName}
            placeholder="Choose your dream place"
            onChangeText={this.props.placeNameHandler}
            />

            <Button 
              title="add" 
              style={styles.ButtonInput}
              onPress={this.props.placeSubmitHandler}  
              />
        </View>
      );
    }
  }

const styles = StyleSheet.create({
    inputBox: {
        //flex: 1,
        width: "100%",
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: "space-between",
      },
    
ButtonInput: {
    width: "30%",
  },
  placeInput: {
    width: "70%",
  },

});

export default PlaceInput;