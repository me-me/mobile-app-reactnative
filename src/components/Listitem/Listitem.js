import React, { Component } from 'react';
import {AppRegistry, View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import ImagePlace from './image.jpg';

class listitem extends Component {
    render() {
      return (
          <TouchableOpacity onPress={this.props.onItemPressed}>
        <View style={styles.listitem}>
        <Image style={styles.listImage} source={ImagePlace} resizeMode="contain"/>
        <Text >
            {this.props.placeName}
        </Text>
        
    </View>
    </TouchableOpacity>
      );
    }
  }

const styles = StyleSheet.create({
    listitem : {
        width: "100%",
        padding: 10,
        marginBottom: 5,
        backgroundColor: "#eee",
        flexDirection: "row",
        alignItems: "center",
    },
    listImage: {
        marginRight: 8,
        height: 30,
        width: 30,
    }
});

export default listitem;